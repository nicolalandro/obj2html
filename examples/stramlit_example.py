import streamlit as st
import streamlit.components.v1 as components
from obj2html import obj2html

# run in the root folder, not inside exaples, to have the relative path working
obj_path = "test/assets/model.obj"
html_string = obj2html(obj_path, html_elements_only=True)
components.html(html_string)
with open(obj_path) as f:
    st.download_button('Download model.obj', f, file_name="model.obj")
